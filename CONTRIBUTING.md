#Contributing to Goblim

##Make a Pull Request

You want to contribute and

* You have a fix for a known issue
* You can improve an existing translation or add a new language.

We accept pull requests!

##Report issue or ask for enhancement

As long as Goblim is developed by Humans, we can make some error and report issues you encounter is very helpful for us. After the report, we can correct it to improve your experience.
You can ask for enhancement too, it is a good thing because users may have better idea than ours on how they imagine the app usage. If the enhancement seems cool and can be developed, we can add it.

##License

Goblim is [licensed under GPLv3](https://github.com/Schoumi/Goblim/tree/master/LICENCE).

