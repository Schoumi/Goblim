# Goblim
[![Build Status](https://drone.mob-dev.fr/api/badges/Schoumi/Goblim/status.svg)](https://drone.mob-dev.fr/Schoumi/Goblim)

Goblim is an Android application.
Goblim let your share your photos on the Lut.im server of your choice. Use your own server to regain control of your privacy!

  * Multiple configurable hosting URLs — Use your own server!
  * Copy the sharing URL to clipboard or share it to another application
  * Keep track of your storage lifetime
  * HTTP & HTTPS support
  * Share history with thumbnails, share control.
  * Encryption of images on the server


Lut.im "Let's Upload That Image" is an open source image sharing server you can install on your own machine (self-hosting, VPS, dedicated, cloud). See https://github.com/ldidry/lutim

Download it on the playstore: https://play.google.com/store/apps/details?id=fr.mobdev.goblim

[![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/Schoumi/donate)
