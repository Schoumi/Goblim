/*
 * Copyright (C) 2015  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.goblim.listener;

import android.net.Uri;

import java.util.EventListener;

import fr.mobdev.goblim.objects.Img;

public interface NetworkListener extends EventListener{
	void fileUploaded(Uri imageUri, Img image);
	void fileUploadError(Uri imageUri, String error);
	void deleteSucceed(String url);
	void deleteError(String error);
	void uploadProgress(int byteWriten, int length, int fileNo, int fileNb);
	void allFileUploaded();
}
