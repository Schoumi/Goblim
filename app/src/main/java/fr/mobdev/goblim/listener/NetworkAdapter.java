/*
 * Copyright (C) 2015  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.goblim.listener;

import android.net.Uri;

import fr.mobdev.goblim.objects.Img;

public class NetworkAdapter  implements NetworkListener{

    @Override
    public void fileUploaded(Uri imageUri, Img image) {
        //do nothing
    }

    @Override
    public void fileUploadError(Uri imageUri, String error) {
        //do nothing
    }

    @Override
    public void deleteSucceed(String deleteUrl) {
        //do nothing
    }

    @Override
    public void deleteError(String Error) {
        //do nothing
    }

    @Override
    public void uploadProgress(int byteWriten, int length, int fileNo, int nbFiles) {
        //do nothing
    }

    @Override
    public void allFileUploaded() {
        //do nothing
    }
}
