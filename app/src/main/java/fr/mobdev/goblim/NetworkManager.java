/*
 * Copyright (C) 2015  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.goblim;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Semaphore;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.Uri;
import android.provider.OpenableColumns;

import fr.mobdev.goblim.listener.NetworkListener;
import fr.mobdev.goblim.objects.Img;
import fr.mobdev.goblim.objects.Server;

public class NetworkManager {

    private NetworkListener listener;
    private static NetworkManager instance;
    private NetworkThread thread;

    private NetworkManager(NetworkListener listener) {
        this.listener = listener;
    }

    public static NetworkManager getInstance(NetworkListener listener) {
        if (instance == null)
            instance = new NetworkManager(listener);
        if (listener != null && listener != instance.getListener())
            instance.setListener(listener);
        return instance;
    }

    private NetworkListener getListener() {
        return listener;
    }

    private void setListener(NetworkListener listener) {
        this.listener = listener;
        if(thread != null)
            thread.setListener(listener);
    }

    public void delete(Context context, String deleteUrl) {
        if (!isConnectedToInternet(context)) {
            listener.fileUploadError(null, context.getString(R.string.no_network));
            return;
        }
        NetworkThread.Message mes = new NetworkThread.Message();
        mes.type = NetworkThread.Message_Type.DELETE_IMG;
        mes.url = deleteUrl;
        mes.context = context;
        addMessageToQueue(mes);
    }

    public void upload(Context context, String siteUrl, int nbDays, List<Uri> imageUris) {
        int i = 1;
        for(Uri uri : imageUris) {
            NetworkThread.Message mes = new NetworkThread.Message();
            mes.type = NetworkThread.Message_Type.UPLOAD_IMG;
            mes.context = context;
            mes.url = siteUrl;
            mes.nbDays = nbDays;
            mes.imageUri = uri;
            mes.fileNb = imageUris.size();
            mes.fileNo = i++;
            addMessageToQueue(mes);
        }
    }

    private void addMessageToQueue(NetworkThread.Message mes){

        if(thread == null || thread.getState() == Thread.State.TERMINATED) {
            thread = new NetworkThread();
            thread.setListener(listener);
            thread.start();
        }
        thread.addMessageQueue(mes);
    }

    public void updateDefaultServersConf(Context context, List<Server> servers) {
        for(Server server : servers) {
            NetworkThread.Message mes = new NetworkThread.Message();
            mes.type = NetworkThread.Message_Type.GET_INFO;
            mes.context = context;
            mes.id = server.getId();
            mes.url = server.getUrl();
            addMessageToQueue(mes);
        }
    }




    private boolean isConnectedToInternet(Context context) {
        //verify the connectivity
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null) {
            State networkState = networkInfo.getState();
            if (networkState.equals(State.CONNECTED)) {
                return true;
            }
        }
        return false;
    }

    public void deleteMultiple(Context context, List<String> urls) {
        for(String url : urls) {
            delete(context,url);
        }
    }
}

class NetworkThread extends Thread{

    private Semaphore sem;
    private List<Message> messages;
    private NetworkListener listener;

    NetworkThread() {
        sem = new Semaphore(0,true);
        messages = new ArrayList<>();
    }

    void setListener(NetworkListener lst){
        listener = lst;
    }

    void addMessageQueue(Message mes) {
        messages.add(mes);
        sem.release();
    }

    @Override
    public void run(){
        boolean isRunning = true;
        while(isRunning) {
            try {
                sem.acquire();
                Message mes = messages.get(0);
                switch (mes.type){
                    case DELETE_IMG:
                        deleteImage(mes.context,mes.url);
                        break;
                    case UPLOAD_IMG:
                        uploadImage(mes.context,mes.url,mes.nbDays,mes.imageUri,mes.fileNo,mes.fileNb);
                        break;
                    case GET_INFO:
                        getInfo(mes.context,mes.url,mes.id);
                        break;
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
                isRunning = false;
            }
            messages.remove(0);
        }
    }

    private void getInfo(Context context, String serverUrl, long id) {
        if (!isConnectedToInternet(context))
            return;
        URL url = null;
        try {
            if(!serverUrl.endsWith("/"))
                serverUrl = serverUrl+"/";
            serverUrl = serverUrl + "infos";
            url = new URL(serverUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (url != null) {
            try {
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                if (urlConnection != null) {
                    InputStream stream = urlConnection.getInputStream();
                    InputStreamReader isr = new InputStreamReader(stream);
                    BufferedReader br = new BufferedReader(isr);
                    boolean isReading = true;
                    String data;
                    String jsonStr = "";
                    //get all data in a String
                    do {
                        try {
                            data = br.readLine();
                            if (data != null)
                                jsonStr += data;
                            else
                                isReading = false;
                        } catch (IOException e) {
                            e.printStackTrace();
                            isReading = false;
                        }
                    } while (isReading);

                    JSONObject rootObject = new JSONObject(jsonStr);
                    if(rootObject.has("default_delay")) {
                        int defaultDelay = rootObject.getInt("default_delay");
                        long maxFileSize = rootObject.getLong("max_file_size");
                        int maxDelay = rootObject.getInt("max_delay");
                        Database.getInstance(context).updateServer(id,defaultDelay,maxDelay,maxFileSize);
                    }

                    stream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void deleteImage(final Context context, final String deleteUrl) {
        if (!isConnectedToInternet(context))
            listener.deleteError(context.getString(R.string.no_network));
        URL url = null;
        try {
            url = new URL(deleteUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (url != null) {
            try {
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                if (urlConnection != null) {
                    InputStream stream = urlConnection.getInputStream();
                    stream.close();
                } else {
                    listener.deleteError(context.getString(R.string.network_error));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            listener.deleteSucceed(deleteUrl);
        }
    }

    private void uploadImage(Context context, String siteUrl, int nbDays, Uri imageUri, int fileNo, int fileNb) {
        if (!isConnectedToInternet(context)) {
            listener.fileUploadError(imageUri,context.getString(R.string.no_network));
            return;
        }

        URL url = null;
        Img imgOutput = null;
        try {
            url = new URL(siteUrl);
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        }

        HttpURLConnection conn = null;
        InputStream stream = null;
        DataOutputStream request = null;
        try {
            if (isConnectedToInternet(context)) {
                String crlf = "\r\n";
                String hyphens = "--";
                String boundary = "------------------------dd8a045fcc22b35c";
                //check if there is a HTTP 301 Error
                if (url != null) {
                    conn = (HttpURLConnection) url.openConnection();
                } else {
                    listener.fileUploadError(imageUri, context.getString(R.string.connection_failed));
                    return;
                }
                String location = conn.getHeaderField("Location");
                if (location != null) {
                    //if there is follow the new destination
                    siteUrl = location;
                    url = new URL(location);
                }
                conn = (HttpURLConnection) url.openConnection();
                //prepare the connection for upload
                conn.setRequestMethod("POST");
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestProperty("User-Agent", "Goblim");

                conn.setRequestProperty("Expect", "100-continue");
                conn.setRequestProperty("Accept", "*/*");
                conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

                int request_size = 0;

                //ask for JSON answer
                String answer = hyphens + boundary + crlf;
                answer += "Content-Disposition: form-data; name=\"format\"" + crlf;
                answer += crlf;
                answer += "json" + crlf;
                request_size += answer.length();

                //ask for storage duration
                String duration = hyphens + boundary + crlf;
                duration += "Content-Disposition: form-data; name=\"delete-day\"" + crlf;
                duration += crlf;
                duration += nbDays + crlf;
                request_size += duration.length();
                String[] proj = {OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE};
                Cursor cursor = context.getContentResolver().query(imageUri, proj, null, null, null);
                String fileName = null;
                long size = 0;
                if (cursor != null && cursor.moveToFirst()) {
                    fileName = cursor.getString(0);
                    size = cursor.getLong(1);
                    cursor.close();
                }

                //setup filename and say that octets follow
                String outputInformations = hyphens + boundary + crlf;
                outputInformations += "Content-Disposition: form-data; name=\"file\"; filename=\"" + fileName + "\"" + crlf;
                outputInformations += "Content-Type: application/octet-stream" + crlf;
                outputInformations += crlf;
                request_size += outputInformations.length();

                request_size += size;

                //finish the format http post packet
                String endHttp = crlf;
                endHttp += hyphens + boundary + hyphens + crlf;
                request_size += endHttp.length();

                conn.setFixedLengthStreamingMode(request_size);

                //write data
                request = new DataOutputStream(conn.getOutputStream());
                request.writeBytes(answer);
                request.writeBytes(duration);
                request.writeBytes(outputInformations);
                request.flush();
                InputStream streamIn = null;
                try {
                    streamIn = context.getContentResolver().openInputStream(imageUri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //read data from the file and write it
                if (streamIn != null) {
                    int readed = 0;
                    int byteWriten = 0;
                    int blockSize = 1024;
                    byte[] buffer = new byte[blockSize];
                    while (readed != -1) {
                        try {
                            readed = streamIn.read(buffer);
                            if (readed != -1) {
                                request.write(buffer, 0, readed);
                                byteWriten += readed;
                                listener.uploadProgress(byteWriten, (int) size, fileNo, fileNb);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            readed = -1;
                        }
                    }
                }
                request.writeBytes(endHttp);
                request.flush();

                //get answer
                stream = conn.getInputStream();
            }
        } catch (IOException e1) {
            if (conn != null) {
                stream = conn.getErrorStream();
            } else {
                e1.printStackTrace();
                listener.fileUploadError(imageUri, context.getString(R.string.network_error));
                return;
            }
        }

        if (stream != null) {
            //prepare JSON reading
            InputStreamReader isr = new InputStreamReader(stream);
            BufferedReader br = new BufferedReader(isr);
            boolean isReading = true;
            String data;
            String jsonStr = "";
            //get all data in a String
            do {
                try {
                    data = br.readLine();
                    if (data != null)
                        jsonStr += data;
                    else
                        isReading = false;
                } catch (IOException e) {
                    e.printStackTrace();
                    isReading = false;
                }
            } while (isReading);
            //parse JSON answer
            try {
                if (request != null)
                    request.close();
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                // Parse the JSON to a JSONObject
                JSONObject rootObject = new JSONObject(jsonStr);
                // Get msg (root) element
                String msgStr = rootObject.getString("msg");
                // is there an error?
                if (!msgStr.contains("{")) {
                    listener.fileUploadError(imageUri, msgStr);
                    return;
                } else if (rootObject.has("msg")) {
                    //retrieve useful data
                    JSONObject msg = rootObject.getJSONObject("msg");
                    String hashOutput = msg.getString("short");
                    String realHashOutput = msg.getString("real_short");
                    String token = msg.getString("token");
                    imgOutput = new Img(0, siteUrl, hashOutput, realHashOutput, Calendar.getInstance(), nbDays, null, token);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                listener.fileUploadError(imageUri, context.getString(R.string.unreadable_json));
                return;
            }
        }
        if(imgOutput != null) {
            listener.fileUploaded(imageUri,imgOutput);
        } else {
            listener.fileUploadError(imageUri, context.getString(R.string.network_error));
        }
    }


    private boolean isConnectedToInternet(Context context) {
        //verify the connectivity
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null) {
            NetworkInfo.State networkState = networkInfo.getState();
            if (networkState.equals(NetworkInfo.State.CONNECTED)) {
                return true;
            }
        }
        return false;
    }

    static class Message{
        Message_Type type;
        Context context;
        int nbDays;
        String url;
        Uri imageUri;
        int fileNo;
        int fileNb;
        long id;
    }

    enum Message_Type{
        DELETE_IMG,
        UPLOAD_IMG,
        GET_INFO
    }
}
