/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.goblim;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ImageListAdapter extends RecyclerView.Adapter<ImageViewHolder> {

    private OnImageClickListener listener;
    private LinkedHashMap<Uri,Bitmap> datas;
    private ImageLoader loader;
    private ImageLoader.ImageLoaderListener loaderListener;

    public ImageListAdapter(OnImageClickListener listener, ImageLoader.ImageLoaderListener loaderListener){
        datas = new LinkedHashMap<>();
        this.listener = listener;
        this.loaderListener = loaderListener;
    }

    public void setBitmapsForUris(Context context, List<Uri> imageUris, List<Bitmap> bitmaps) {
        int i = 0;
        if(bitmaps.size() != imageUris.size())
            return;
        for(Uri imUri : imageUris) {
            setBitmapForUri(imUri,bitmaps.get(i));
            if(bitmaps.get(i) == null) {
                ImageLoader.Message mes = new ImageLoader.Message();
                mes.context = context;
                mes.uri = imUri;
                addMessageToQueue(mes);
            }
            i++;

        }
    }

    public void setBitmapForUri(Uri imageUri, Bitmap thumb) {
        if(datas.containsKey(imageUri)) {
            datas.put(imageUri, thumb);
            notifyDataSetChanged();
        }
    }

    public void addUris(Context context, List<Uri> uris, boolean loadImg) {
        for(Uri imUri : uris) {
            if(loadImg) {
                addUri(context,imUri);
            } else {
                datas.put(imUri,null);
            }
        }
        notifyDataSetChanged();
    }

    public void addUri(Context context, Uri imUri) {
        datas.put(imUri,null);
        ImageLoader.Message mes = new ImageLoader.Message();
        mes.context = context;
        mes.uri = imUri;
        addMessageToQueue(mes);
        notifyDataSetChanged();
    }

    private void addMessageToQueue(ImageLoader.Message mes) {
        if(loader == null || loader.getState() == Thread.State.TERMINATED) {
            loader = new ImageLoader(loaderListener);
            loader.start();
        }
        loader.addMessageToQueue(mes);
    }

    public Bitmap getForUri(Uri imUri) {
        return datas.get(imUri);
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.thumb_items,parent,false);
        return new ImageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        List<Uri> uris = new ArrayList<>(datas.keySet());
        final Uri imUri = uris.get(position);
        Bitmap bt = datas.get(imUri);
        if(bt != null) {
            holder.thumb.setVisibility(View.VISIBLE);
            holder.thumb.setImageBitmap(bt);
            holder.progressLoad.setVisibility(View.GONE);
        } else {
            holder.thumb.setVisibility(View.GONE);
            holder.progressLoad.setVisibility(View.VISIBLE);
        }
        holder.resetBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datas.remove(imUri);
                listener.onRemove(imUri);
                notifyDataSetChanged();
            }
        });
        holder.thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnImageClick(imUri);
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public ArrayList<Bitmap> getBitmaps() {
        return new ArrayList<>(datas.values());
    }

    public void clear() {
        datas.clear();
        notifyDataSetChanged();
    }

    public interface OnImageClickListener{
        void OnImageClick(Uri imageUri);
        void onRemove(Uri imageUri);
    }
}

class ImageViewHolder extends RecyclerView.ViewHolder {
    ProgressBar progressLoad;
    ImageView resetBt;
    ImageView thumb;
    ImageViewHolder(View v) {
        super(v);
        resetBt = (ImageView) v.findViewById(R.id.reset_img);
        thumb = (ImageView) v.findViewById(R.id.thumbnail_item);
        progressLoad = (ProgressBar) v.findViewById(R.id.progress_thumb);
    }
}

