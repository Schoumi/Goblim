/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.goblim;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MultiLinkAdapter extends RecyclerView.Adapter<MultiLinkViewHolder>{

    private List<Bitmap> bitmaps;
    private List<Integer> selecteds;
    private SelectionChangeListener selectionListener;

    public MultiLinkAdapter(int length, SelectionChangeListener listener) {
        selectionListener = listener;
        bitmaps = new ArrayList<>();
        selecteds = new ArrayList<>();
        for(int i = 0; i < length; i++) {
            bitmaps.add(null);
            selecteds.add(i);
        }
    }

    public void setBitmap(Bitmap bt, int position) {
        bitmaps.set(position,bt);
        notifyDataSetChanged();
    }

    public List<Integer> getSelecteds() {
        return selecteds;
    }

    @Override
    public MultiLinkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.link_item,parent,false);
        final MultiLinkViewHolder holder = new MultiLinkViewHolder(v);
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox box;
                if(v instanceof CheckBox) {
                    box = (CheckBox) v;
                } else {
                    box = (CheckBox) v.findViewById(R.id.selected);
                }
                if(box.isChecked() && !selecteds.contains(holder.index)) {
                    selecteds.set(holder.index,holder.index);
                } else if (!box.isChecked() && selecteds.contains(holder.index)) {
                    selecteds.set(holder.index,-1);
                }
                selectionListener.onSelectionChanged();
            }
        };

        CheckBox box = (CheckBox) v.findViewById(R.id.selected);
        box.setOnClickListener(listener);
        v.setOnClickListener(listener);

        return holder;
    }

    @Override
    public void onBindViewHolder(MultiLinkViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Bitmap bt = bitmaps.get(position);
        if(bt != null) {
            holder.progress.setVisibility(View.GONE);
            holder.thumb.setImageBitmap(bt);
            holder.thumb.setVisibility(View.VISIBLE);
        } else {
            holder.progress.setVisibility(View.VISIBLE);
            holder.thumb.setVisibility(View.GONE);
        }

        holder.box.setChecked(selecteds.get(position) != -1);
        holder.index = position;
    }

    @Override
    public int getItemCount() {
        return bitmaps.size();
    }

    public interface SelectionChangeListener {
        void onSelectionChanged();
    }
}

class MultiLinkViewHolder extends RecyclerView.ViewHolder {

    int index;
    CheckBox box;
    ImageView thumb;
    ProgressBar progress;

    MultiLinkViewHolder(View itemView) {
        super(itemView);
        box = (CheckBox) itemView.findViewById(R.id.selected);
        thumb = (ImageView) itemView.findViewById(R.id.thumbnail_link);
        progress = (ProgressBar) itemView.findViewById(R.id.progress);
    }
}