/*
 * Copyright (C) 2015  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.goblim.activity;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.mobdev.goblim.Database;
import fr.mobdev.goblim.ImageListAdapter;
import fr.mobdev.goblim.ImageLoader;
import fr.mobdev.goblim.listener.NetworkAdapter;
import fr.mobdev.goblim.NetworkManager;
import fr.mobdev.goblim.objects.Img;
import fr.mobdev.goblim.objects.Server;
import fr.mobdev.goblim.R;

/*
 * Activity used to handle sharing pictures from other app that user want to upload on a lutim instance
 * if user launch the app by itself he can also pick a pictures from his device and upload it as well.
 * This Activity let user access to the others activities in order to manage history, servers and after an upload
 * the shared options of the given link
 */
public class UploadActivity extends AppCompatActivity {

    private NetworkAdapter listener;
    private ArrayList<Uri> uris;
    private List<Server> servers;
    private List<String> urls;
    private List<Integer> deletedDays;
    private ProgressDialog progressDialog;
    private ImageListAdapter adapter;
    private boolean uploadInProgress;

    //static value to handle storage durations options
    private static final int NEVER = 0;
    private static final int ONE = 1;
    private static final int SEVEN = 7;
    private static final int THIRTY = 30;
    private static final int YEAR = 365;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload);
        uploadInProgress = false;

        setTitle(R.string.upload_pict);
        hideImage();

        //prepare data used for upload
        uris = new ArrayList<>();
        urls = new ArrayList<>();
        deletedDays = new ArrayList<>();
        deletedDays.add(NEVER);
        deletedDays.add(ONE);
        deletedDays.add(SEVEN);
        deletedDays.add(THIRTY);
        deletedDays.add(YEAR);


        final LinearLayout infosLayout = (LinearLayout) findViewById(R.id.info_layout);
        FrameLayout dataLayout = (FrameLayout) findViewById(R.id.data_layout);
        dataLayout.setVisibility(View.GONE);

        ImageView imView = (ImageView) findViewById(R.id.thumbnail_main);
        imView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideImage();
            }
        });

        ImageListAdapter.OnImageClickListener onClickListener = new ImageListAdapter.OnImageClickListener() {
            @Override
            public void OnImageClick(Uri imageUri) {
                displayImage(adapter.getForUri(imageUri));
            }

            @Override
            public void onRemove(Uri imageUri) {
                resetImage(imageUri);
            }
        };
        ImageLoader.ImageLoaderListener loaderListener = new ImageLoader.ImageLoaderListener() {
            @Override
            public void thumbLoaded(final Uri imageUri, final Bitmap thumb) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.setBitmapForUri(imageUri,thumb);
                    }
                });
            }

            @Override
            public void allThumbLoaded() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Button send = (Button) findViewById(R.id.upload_button);
                        send.setEnabled(true);
                    }
                });
            }
        };
        adapter = new ImageListAdapter(onClickListener,loaderListener);

        updateServerList();
        RecyclerView th = (RecyclerView) findViewById(R.id.imageList);
        th.setAdapter(adapter);
        Button uploadBt = (Button) findViewById(R.id.upload_button);

        Spinner serverSpinner = (Spinner) findViewById(R.id.servers_spinner);
        serverSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Server server = servers.get(position);
                int defaultDelay = server.getDefautDelay();
                int pos = 0;
                int min = Integer.MAX_VALUE;
                int savePos = 0;
                for(Integer delay : deletedDays) {
                    if(delay == defaultDelay)
                        break;
                    int diff = delay - defaultDelay;
                    if(min > Math.abs(diff)) {
                        min = Math.abs(diff);
                        savePos = pos;
                    }
                    pos++;
                }
                Spinner deleteDaySpinner = (Spinner) findViewById(R.id.delete_day_spinner);
                if(pos >= deletedDays.size())
                    deleteDaySpinner.setSelection(savePos);
                else
                    deleteDaySpinner.setSelection(pos);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //retrieve previous state if it exist
        if(savedInstanceState != null) {
            int selectedServer = savedInstanceState.getInt("selectedServer");
            uris = savedInstanceState.getParcelableArrayList("imageURI");
            ArrayList<String> cacheUris = savedInstanceState.getStringArrayList("thumb");
            List<Bitmap> bitmaps = loadFromCache(cacheUris);
            if(bitmaps != null) {
                adapter.addUris(this,uris,false);
                adapter.setBitmapsForUris(this,uris,bitmaps);
                boolean allLoaded = true;
                for(Bitmap bitmap : bitmaps){
                    if(bitmap == null)
                        allLoaded = false;
                }
                uploadBt.setEnabled(allLoaded);

                infosLayout.setVisibility(View.GONE);
                dataLayout.setVisibility(View.VISIBLE);
            }

            if (selectedServer < urls.size()) {
                serverSpinner.setSelection(selectedServer);
            }
        }
        deleteCache();

        //prepare the listener that handle upload result
        listener = new NetworkAdapter() {

            List<Long> results = new ArrayList<>();
            List<Uri> fileInError = new ArrayList<>();
            List<String> errorMsg = new ArrayList<>();

            @Override
            public void fileUploaded(final Uri imageUri,final Img image) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                        adapter.getForUri(imageUri).compress(Bitmap.CompressFormat.JPEG,70,outputStream);
                        image.setThumbData(outputStream.toByteArray());

                        //add uploaded img to history
                        Long id = Database.getInstance(getApplicationContext()).addImage(image);
                        results.add(id);
                        if(results.size() + fileInError.size() == uris.size())
                            allFileUploaded();
                    }
                });
            }

            @Override
            public void fileUploadError(final Uri imageUri, final String error) {
                fileInError.add(imageUri);
                errorMsg.add(error);
                if(results.size() + fileInError.size() == uris.size())
                    allFileUploaded();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //display toast error
                        Toast.makeText(UploadActivity.this, error, Toast.LENGTH_SHORT).show();
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                    }
                });
            }

            @Override
            public void uploadProgress(final int progress, final int length, final int fileNo, final int nbFiles) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(progressDialog != null) {
                            progressDialog.setMessage(getString(R.string.uploading_files)+" "+fileNo+"/"+nbFiles);
                            if(length != 0)
                                progressDialog.setProgress(progress * 100 / length);
                        }
                    }
                });
            }

            @Override
            public void allFileUploaded() {
                uploadInProgress = false;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                            progressDialog = null;
                        }
                        if(!fileInError.isEmpty()) {
                            String failed = "";
                            String[] proj = {OpenableColumns.DISPLAY_NAME};
                            for(int i = 0; i < fileInError.size(); i++) {
                                Cursor cursor = getContentResolver().query(fileInError.get(i), proj, null, null, null);
                                String fileName = null;
                                if (cursor != null && cursor.moveToFirst()) {
                                    fileName = cursor.getString(0);
                                    cursor.close();
                                }
                                if(fileName == null)
                                    fileName="File "+i;
                                failed += fileName;
                                failed += "\n\t";
                                failed += errorMsg.get(i);
                                failed += "\n";
                            }
                            AlertDialog.Builder builder = new AlertDialog.Builder(UploadActivity.this);
                            String message;
                            if(fileInError.size() > 1)
                                message = getString(R.string.retry_failed_uploads);
                            else
                                message = getString(R.string.retry_failed_upload);
                            builder.setMessage(message+"\n"+failed)
                                   .setCancelable(false)
                                   .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                       public void onClick(DialogInterface dialog, int id) {
                                           uploadImages(fileInError);
                                           fileInError.clear();
                                           errorMsg.clear();
                                           dialog.dismiss();
                                       }
                                   })
                                   .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                       public void onClick(DialogInterface dialog, int id) {
                                           startLinkActivity(true);
                                           fileInError.clear();
                                           errorMsg.clear();
                                           dialog.cancel();
                                       }
                                   });
                            AlertDialog alert = builder.create();
                            alert.show();
                        } else {
                            startLinkActivity(false);
                        }
                    }
                });
            }

            private void startLinkActivity(boolean force) {
                Intent linkIntent = null;
                if(results.size() == 1 && (fileInError.isEmpty() || force)) {
                    //launch LinkActivity
                    linkIntent = new Intent(UploadActivity.this, LinkActivity.class);
                    linkIntent.putExtra("imageId", results.get(0));
                } else if(results.size() > 1 && (fileInError.isEmpty() || force)) {
                    linkIntent = new Intent(UploadActivity.this,MultiLinkActivity.class);
                    linkIntent.putExtra("imageIds", results.toArray());
                }
                if(linkIntent != null) {
                    resetImages();
                    fileInError.clear();
                    errorMsg.clear();
                    results.clear();
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                    startActivity(linkIntent);
                }
            }
        };

        //prepare for upload
        uploadBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
                        uploadImages(uris);
                    }
                }).start();
            }
        });

        infosLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestFile();
            }
        });

        //have we receive image from share or do you need to ask it to the user if we haven't ask for it before (screen rotation)
        Intent receiveIntent = getIntent();
        if((receiveIntent == null || receiveIntent.getType() == null || !receiveIntent.getType().contains("image/")) && uris.isEmpty()) {
            uploadBt.setEnabled(false);
        }
        else {
            if(receiveIntent != null && receiveIntent.getAction() != null) {
                if(receiveIntent.getAction().equals(Intent.ACTION_SEND)) {
                    Uri imUri = receiveIntent.getParcelableExtra(Intent.EXTRA_STREAM);
                    uris.add(imUri);
                    adapter.addUri(this,imUri);
                    uploadBt.setEnabled(false);
                    infosLayout.setVisibility(View.GONE);
                    dataLayout.setVisibility(View.VISIBLE);
                } else if(receiveIntent.getAction().equals(Intent.ACTION_SEND_MULTIPLE)) {
                    ArrayList<Uri> imUris = receiveIntent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
                    if(imUris != null) {
                        uris.addAll(imUris);
                        adapter.addUris(this,imUris,true);
                        uploadBt.setEnabled(false);
                        infosLayout.setVisibility(View.GONE);
                        dataLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
        updateSpanCount();
    }



    @Override
    protected void onResume() {
        super.onResume();
        //update server list to manage change in ServerActivity
        updateServerList();
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        //save imageURI and selected server position
        Spinner selectedServer = (Spinner) findViewById(R.id.servers_spinner);
        int pos = selectedServer.getSelectedItemPosition();
        savedInstanceState.putInt("selectedServer", pos);
        savedInstanceState.putParcelableArrayList("imageURI", uris);
        ArrayList<String> cacheUris = writesToCache(adapter.getBitmaps());
        savedInstanceState.putStringArrayList("thumb",cacheUris);
        super.onSaveInstanceState(savedInstanceState);
    }

    private ArrayList<String> writesToCache(List<Bitmap> bitmaps){
        ArrayList<String> paths = new ArrayList<>();
        File thumbDir = new File(getCacheDir(),"thumbs");
        boolean exist = thumbDir.exists();
        if(!exist) {
            exist = thumbDir.mkdir();
        }
        if(exist)
        {
            for(Bitmap bt : bitmaps) {
                if(bt == null) {
                    paths.add(null);
                    continue;
                }
                try {
                    File thumb = File.createTempFile("File","tmp",thumbDir);
                    if(thumb.exists()) {
                        paths.add(thumb.getPath());
                        FileOutputStream stream = new FileOutputStream(thumb);
                        bt.compress(Bitmap.CompressFormat.PNG,100,stream);
                    }
                }catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return paths;
    }

    private List<Bitmap> loadFromCache(List<String> paths) {
        List<Bitmap> bitmaps = new ArrayList<>();
        for(String path : paths) {
            if(path != null) {
                Bitmap bt = BitmapFactory.decodeFile(path);
                bitmaps.add(bt);
            } else {
                bitmaps.add(null);
            }
        }
        return bitmaps;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void deleteCache() {
        File thumbDir = new File(getCacheDir()+"thumb");
        if(thumbDir.exists())
            thumbDir.delete();
    }

    private void updateServerList() {
        Spinner serversSpinner = (Spinner) findViewById(R.id.servers_spinner);
        //retrieve the selected server name in case it change his place in list
        String selectedServer = (String) serversSpinner.getSelectedItem();

        servers = Database.getInstance(getApplicationContext()).getServers(true);
        urls.clear();
        int pos = 0;
        //create the string list of server name from database data
        for(Server server : servers) {
            if(server.getUrl().equals(selectedServer)) {
                pos = urls.size();
            }
            urls.add(server.getUrl());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_dropdown_item_1line,urls);
        serversSpinner.setAdapter(adapter);

        //select the previous selected server
        serversSpinner.setSelection(pos);
    }

   private void displayImage(Bitmap bt) {
        if(bt != null) {
            //display it in the imageView
            final ImageView view = (ImageView) findViewById(R.id.thumbnail_main);
            view.setVisibility(View.VISIBLE);
            view.setImageBitmap(bt);
        }
    }

    private void hideImage() {
        final ImageView view = (ImageView) findViewById(R.id.thumbnail_main);
        view.setVisibility(View.GONE);
    }

    private void uploadImages(List<Uri> imageUris) {
        if(uploadInProgress)
            return;
        uploadInProgress = true;
        //what server we use
        Spinner urlSpinner = (Spinner)findViewById(R.id.servers_spinner);
        int pos = urlSpinner.getSelectedItemPosition();
        if(urls.size() < pos || pos == Spinner.INVALID_POSITION) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(UploadActivity.this, getString(R.string.server_list_error), Toast.LENGTH_LONG).show();
                }
            });
            uploadInProgress = false;
            return;
        }
        String url = urls.get(pos);

        //How long the server need to store image
        Spinner deleteSpinner = (Spinner)findViewById(R.id.delete_day_spinner);
        pos = deleteSpinner.getSelectedItemPosition();
        int delete = deletedDays.get(pos);

        //read image as bytes to upload it
        //create a fileStream from the file path
        //upload image and display a progress bar
        runOnUiThread(new Runnable() {
                @Override
                public void run() {
                progressDialog = new ProgressDialog(UploadActivity.this);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog.setMax(100);
                progressDialog.setMessage(getString(R.string.upload_progress));
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
            });
        NetworkManager.getInstance(listener).upload(this,url, delete, imageUris);
    }

    private void resetImage(Uri imageUri) {
        uris.remove(imageUri);
        ImageView view = (ImageView) findViewById(R.id.thumbnail_main);
        view.setImageBitmap(null);
        view.setVisibility(View.GONE);
        Button bt = (Button) findViewById(R.id.upload_button);
        if (bt.isEnabled() && uris.size() == 0) {
            bt.setEnabled(false);
        }
        if(uris.size() == 0) {
            LinearLayout infosLayout = (LinearLayout) findViewById(R.id.info_layout);
            infosLayout.setVisibility(View.VISIBLE);

            FrameLayout dataLayout = (FrameLayout) findViewById(R.id.data_layout);
            dataLayout.setVisibility(View.GONE);
        }
        updateSpanCount();
    }

    private void resetImages(){
        uris.clear();
        adapter.clear();
        ImageView view = (ImageView) findViewById(R.id.thumbnail_main);
        view.setImageBitmap(null);
        view.setVisibility(View.GONE);
        Button bt = (Button) findViewById(R.id.upload_button);
        bt.setEnabled(false);
        updateSpanCount();
        LinearLayout infosLayout = (LinearLayout) findViewById(R.id.info_layout);
        infosLayout.setVisibility(View.VISIBLE);

        FrameLayout dataLayout = (FrameLayout) findViewById(R.id.data_layout);
        dataLayout.setVisibility(View.GONE);
    }

    private void requestFile() {
        //ask for image file
        Intent requestFileIntent = new Intent(Intent.ACTION_PICK);
        requestFileIntent.setType("image/*");
        requestFileIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
        startActivityForResult(requestFileIntent, 0);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,Intent returnIntent) {
        super.onActivityResult(requestCode, resultCode, returnIntent);
        if (resultCode == RESULT_OK) {
            //retrieve uri from the request image activity and prepare
            ClipData clip = null;
            boolean hasAddUri = false;
            clip = returnIntent.getClipData();
            LinearLayout infosLayout = (LinearLayout) findViewById(R.id.info_layout);
            FrameLayout dataLayout = (FrameLayout) findViewById(R.id.data_layout);
            if (clip == null) {
                Uri imageUri = returnIntent.getData();
                if (imageUri == null)
                    return;
                uris.add(imageUri);
                hasAddUri = true;
                updateSpanCount();
                adapter.addUri(this, imageUri);

                infosLayout.setVisibility(View.GONE);
                dataLayout.setVisibility(View.VISIBLE);
            } else {
                for (int i = 0; i < clip.getItemCount(); i++) {
                    Uri imageUri = clip.getItemAt(i).getUri();
                    if (imageUri == null)
                        continue;
                    uris.add(imageUri);
                    hasAddUri = true;
                    updateSpanCount();
                    adapter.addUri(this, imageUri);

                    infosLayout.setVisibility(View.GONE);
                    dataLayout.setVisibility(View.VISIBLE);

                }
            }

            if (hasAddUri) {
                Button uploadBt = (Button) findViewById(R.id.upload_button);
                uploadBt.setEnabled(false);
            }
        }
    }

    private void updateSpanCount() {
        RecyclerView view = (RecyclerView) findViewById(R.id.imageList);
        GridLayoutManager manager = (GridLayoutManager) view.getLayoutManager();
        if(uris.size() > 1) {
            manager.setSpanCount(2);
        } else {
            manager.setSpanCount(1);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_upload, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent newIntent = null;
        if (id == R.id.action_manage_server) {
            newIntent = new Intent(this,ServersActivity.class);
        } else if(id == R.id.action_add_image) {
            requestFile();
        } else if(id == R.id.action_clear) {
            resetImages();
        }
        if(newIntent != null)
        {
            startActivity(newIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
