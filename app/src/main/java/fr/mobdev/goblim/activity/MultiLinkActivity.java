/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.goblim.activity;

import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.mobdev.goblim.Database;
import fr.mobdev.goblim.MultiLinkAdapter;
import fr.mobdev.goblim.NetworkManager;
import fr.mobdev.goblim.R;
import fr.mobdev.goblim.listener.NetworkAdapter;
import fr.mobdev.goblim.objects.Img;

public class MultiLinkActivity extends AppCompatActivity {

    private ArrayList<String> sharedHashs;
    private List<String> deleteUrls;
    private MultiLinkAdapter adapter;
    private List<Img> images;
    private String baseUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.multilink);

        //get url information
        Intent receiveIntent = getIntent();
        Object[] extra = (Object[]) receiveIntent.getSerializableExtra("imageIds");

        final Long[] ids = Arrays.copyOf(extra,extra.length,Long[].class);

        final MultiLinkAdapter.SelectionChangeListener selectionListener = new MultiLinkAdapter.SelectionChangeListener() {
            @Override
            public void onSelectionChanged() {
                String generatedSharedLink = generateShardedLink();
                TextView tv = (TextView) findViewById(R.id.link);
                tv.setText(generatedSharedLink);
            }
        };
        TextView link = (TextView) findViewById(R.id.link);
        Typeface typeface = Typeface.createFromAsset(getAssets(),"fonts/NotoSans-Regular.ttf");
        link.setTypeface(typeface);

        //setup Adapter
        adapter = new MultiLinkAdapter(ids.length,selectionListener);
        RecyclerView listView = (RecyclerView) findViewById(R.id.link_list);
        listView.setAdapter(adapter);

        sharedHashs = new ArrayList<>();
        deleteUrls = new ArrayList<>();
        images = new ArrayList<>();

        final ImageButton shareButton = (ImageButton) findViewById(R.id.share_button);
        final ImageButton copyClipboardButton = (ImageButton) findViewById(R.id.copy_clipboard_button);
        final ImageButton deleteImageButton = (ImageButton) findViewById(R.id.delete_button);
        shareButton.setEnabled(false);
        copyClipboardButton.setEnabled(false);
        deleteImageButton.setEnabled(false);

        //TODO Save For Rotation

        new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                for(Long id : ids) {
                    Img image = Database.getInstance(getApplicationContext()).getImage(id);
                    images.add(image);
                    String url = image.getUrl();
                    baseUrl = url;
                    String shortHash = image.getShortHash();
                    String realShortHash = image.getRealShortHash();
                    String token = image.getToken();
                    final Bitmap bt = image.getThumb();
                    final int index = i;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.setBitmap(bt,index);
                        }
                    });
                    //add a / at the end of the url before adding the hash
                    if(!url.endsWith("/")) {
                        url = url.concat("/");
                        baseUrl = baseUrl.concat("/");
                    }
                    baseUrl = baseUrl.concat("gallery#");
                    String deleteUrl = url.concat("d/"+realShortHash+"/"+token);

                    sharedHashs.add(shortHash);
                    deleteUrls.add(deleteUrl);
                    i++;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        shareButton.setEnabled(true);
                        copyClipboardButton.setEnabled(true);
                        deleteImageButton.setEnabled(true);
                        selectionListener.onSelectionChanged();
                    }
                });

            }
        }).start();





        //manage the sharing button
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
                String output = generateShardedLink();
                sendIntent.putExtra(Intent.EXTRA_TEXT, output);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        //manage the clipboard button
        copyClipboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                String output = generateShardedLink();
                android.content.ClipData clip = android.content.ClipData.newPlainText("Copied URL", output);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(MultiLinkActivity.this,getString(R.string.copy_to_clipboard),Toast.LENGTH_SHORT).show();
            }
        });

        final NetworkAdapter listener = new NetworkAdapter() {

            @Override
            public void deleteSucceed(String deleteUrl)
            {
                List<Img> imgs = new ArrayList<>();
                int idx = deleteUrls.indexOf(deleteUrl);
                if(idx < 0 || idx >= images.size())
                    return;
                imgs.add(images.get(idx));
                Database.getInstance(getApplicationContext()).deleteImg(imgs);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MultiLinkActivity.this, R.string.delete_succeed, Toast.LENGTH_SHORT).show();
                    }
                });
                finish();
            }

            @Override
            public void deleteError(final String error)
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MultiLinkActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };

        deleteImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MultiLinkActivity.this);
                builder.setMessage(getString(R.string.delete_selected_image))
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                List<Integer> selected = adapter.getSelecteds();
                                List<String> urls = new ArrayList<>();
                                for(Integer index : selected) {
                                    if(index != -1)
                                        urls.add(deleteUrls.get(index));
                                }
                                NetworkManager.getInstance(listener).deleteMultiple(MultiLinkActivity.this, urls);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    private String generateShardedLink() {
        List<Integer> selected = adapter.getSelecteds();
        String output = baseUrl;
        for(Integer index : selected) {
            if(index != -1)
                output += sharedHashs.get(index)+",";
        }
        return output;
    }
}
