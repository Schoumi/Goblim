/*
 * Copyright (C) 2017  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.goblim;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.DisplayMetrics;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class ImageLoader extends Thread {

    private Semaphore sem;
    private List<Message> messages;
    private ImageLoaderListener listener;

    ImageLoader(ImageLoaderListener listener) {
        sem = new Semaphore(0,true);
        messages = new ArrayList<>();
        this.listener = listener;
    }

    void addMessageToQueue(Message mes) {
        messages.add(mes);
        sem.release();
    }

    @Override
    public void run(){
        boolean isRunning = true;
        while (isRunning){
            try {
                sem.acquire();
                if (messages.size() > 0) {
                    Message mes = messages.get(0);
                    Bitmap thumb = loadThumb(mes);
                    if(thumb != null) {
                        listener.thumbLoaded(mes.uri, thumb);
                    }
                }
            }catch(InterruptedException e){
                e.printStackTrace();
                isRunning = false;
            }
            messages.remove(0);
            if(messages.isEmpty())
                listener.allThumbLoaded();
        }
    }

    private Bitmap loadThumb(Message mes) {
        Bitmap thumb = null;
        try {
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inScaled = true;
            opts.inDensity = 640;
            opts.inTargetDensity = DisplayMetrics.DENSITY_MEDIUM;
            thumb = BitmapFactory.decodeStream(mes.context.getContentResolver().openInputStream(mes.uri), null, opts);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return thumb;
    }

    static class Message {
        Context context;
        Uri uri;
    }

    public interface ImageLoaderListener{
        void thumbLoaded(Uri imageUri, Bitmap thumb);
        void allThumbLoaded();
    }
}
